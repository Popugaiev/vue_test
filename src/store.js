import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
const items = [];
const lines = 25;
const columns = 15;
const windowSizes = {};
//item size
const itemSizes = {
  width: 180,
  height: 45
};
//items quantity in lines and columns
const full = {
  lines: 40000,
  columns: 40000
};

const scroll = {
  vert: 0,
  right: 0
};
export default new Vuex.Store({
  state: {
    items,
    lines,
    columns,
    windowSizes,
    itemSizes,
    full,
    scroll
  },
  mutations: {
    changeV(state, payload) {
      //save element changes to store
        let el = state.items.find((el) => {
          return el.lineId == payload.lineId && el.itemId == payload.itemId
        });
        if (el) {
          el.value = payload.value ? payload.value : el.value;
          el.checked = payload.hasOwnProperty('checked') ? payload.checked : el.checked;
          el.changed = payload.changed;
        } else {
          state.items.push(payload);
        }
    },
    save(state, payload) {
      //save button
      let el = state.items.find((el) => {
        return el.lineId == payload.lineId && el.itemId == payload.itemId
      });
      el.changed = false;
    },
    windowSizes(state, sizes) {
      state.windowSizes = sizes;
      state.lines = Math.floor(state.windowSizes.height / state.itemSizes.height) + 1;
      state.columns = Math.floor(state.windowSizes.width / state.itemSizes.width) + 1;
    },
    scroll(state, scroll){
      //calculates where we are in document 
      state.scroll.vert = Math.round(scroll.y / state.itemSizes.height);
      state.scroll.right = Math.round(scroll.x / state.itemSizes.width);
    }
  },
});